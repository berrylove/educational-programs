import instance from '../utils/myhttp.js'
// 获取商品数据
function sysLogin (data) {
  return instance({
    url: '/sys/login',
    method: 'post',
    data
  })
}
export { sysLogin }
