import axios from '../utils/httpRequest'
// axios的参数有两种写法
// - data ：参数会在请求体中传递。
//          如果接口文档中是用Body来说明参数，则用data
// - params：参数会在请求行中传递。类似于查询字符串的结构。
//          如果接口文档中是用query来说明参数，则用params

/**
 * 上传成绩分页
 * @param {*} data
 * data包含： 
 * @param {*} orgId // 组织Id
 * @param {*} sex // 性别
 * @param {*} pageNo // 页码
 * @param {*} pageSize // 每页条数
 */
export const upResults = (data) => {
    return axios({
        method: 'GET',
        url: axios.wkbbBaseUrl('achievement/getAchievementPageList'),
        params: data
    })
}
/**
 * 下载成绩模板
 * @param {*} data 
 */
export const dowmExcel = (data) => {
    return axios({
        method: 'GET',
        responseType: 'blob',
        headers: {
            "Accept": "*/*"
        },
        url: axios.wkbbBaseUrl('achievement/downloadAchievementTmeplateExcel'),
        params: data
    })
}

/**
 * 下载成绩
 * @param {*} data 
 */
export const dowmAchievement = (data) => {
    return axios({
        method: 'GET',
        responseType: 'blob',
        headers: {
            "Accept": "*/*"
        },
        url: axios.wkbbBaseUrl('achievement/downloadAchievementExcel'),
        params: data
    })
}

/**
 * 获取教师课程首页列表
 * @param {*} offset   起始记录
 * @param {*} pageSize 每页多少条数据
 */
export const getTchCourseInfoTable = (data) => {
    return axios({
        method: 'POST',
        url: axios.adornUrl('queryCourseInfo'),
        data: data
    })
}

/**
 * 获取体质列表
 * @param {*} offset   起始记录
 * @param {*} pageSize 每页多少条数据
 */
export const tizhitext = (data) => {
    return axios({
        method: 'GET',
        url: axios.wkbbBaseUrl('physical/getPhysicalPageList'),
        params: data

    })
}
/**
 * 体质新增
 * @param {*} offset   起始记录
 * @param {*} pageSize 每页多少条数据
 */
export const addPhysical = (data) => {
    return axios({
        method: 'POST',
        url: axios.wkbbBaseUrl('physical/addPhysical'),
        data: data

    })
}
/**
 * 体质修改
 * @param {*} offset   起始记录
 * @param {*} pageSize 每页多少条数据
 */
export const editPhysical = (data) => {
    return axios({
        method: 'PUT',
        url: axios.wkbbBaseUrl('physical/updatePhysical'),
        data: data

    })
}
/**
 * 体质列表删除
 * @param {*} offset   起始记录
 * @param {*} pageSize 每页多少条数据
 */
export const delete_tizi = (data) => {
    return axios({
        method: 'DELETE',
        url: axios.wkbbBaseUrl('physical/delPhysical'),
        params: data
    })
}
/**
 * 修改体质信息
 * @param {*} offset   起始记录
 * @param {*} pageSize 每页多少条数据
 */
export const update_tizi = (data) => {
    data.queryParam = JSON.parse(sessionStorage.getItem('userMsg'));
    return axios({
        method: 'POST',
        url: axios.adornUrl('updatePhysical'),
        data: data

    })
}
/**
 * 体质 考察点列表
 * @param {*} pageNo   页码
 * @param {*} pageSize 每页多少条数据
 */

export const get_investigate_list = (data) => {
    return axios({
        method: 'GET',
        url: axios.wkbbBaseUrl('common/getTargetList'),
        params: data
    })
}
/**
 * 体质 体质详情
 * @param {*} pageNo   页码
 * @param {*} pageSize 每页多少条数据
 */

export const get_tizhi_details = (data) => {
    return axios({
        method: 'GET',
        url: axios.wkbbBaseUrl(`physical/getPhysical/${data.physicalId}?orgId=${data.orgId}`)
    })
}

/**
 * 根据课程ID删除课程
 * @param {*} courseId 课程ID
 */
export const delCourse = (courseId) => {
    return axios({
        method: 'POST',
        url: axios.adornUrl(`delCourse/` + courseId),
        queryParam: JSON.parse(sessionStorage.getItem('userMsg'))
    })
}
/**
 * 套餐列表删除
 * @param {*} offset   起始记录
 * @param {*} pageSize 每页多少条数据
 */
export const delete_setmeal = (id) => {
    var data = {};
    data.queryParam = JSON.parse(sessionStorage.getItem('userMsg'));
    return axios({
        method: 'POST',
        url: axios.adornUrl('deleteSetMeal/' + id),
        data: data

    })
}

/**
 * 获取体检列表
 * @param {*} offset   起始记录
 * @param {*} pageSize 每页多少条数据
 * @param {*} data {offset: '', pageSize: '', queryParam}
 */
export const health = (data) => {
    return axios({
        method: 'GET',
        url: axios.wkbbBaseUrl('examine/getExaminePageList'),
        params: data
    })
}

/**
 * 体检新增
 * @param {*} data 
 */
export const addExamine = (data) => {
    return axios({
        method: 'POST',
        url: axios.wkbbBaseUrl('examine/addExamine'),
        data: data
    })
}

/**
 * 获取套餐列表
 * @param {*} offset   起始记录
 * @param {*} pageSize 每页多少条数据
 * @param {*} data {offset: '', pageSize: '', queryParam}
 */
export const food_text = (data) => {
    data.queryParam = JSON.parse(sessionStorage.getItem('userMsg'));
    return axios({
        method: 'POST',
        url: axios.adornUrl('queryPageSetMeal'),
        data: data
    })
}

/**
 * 获取套餐详情
 * @param {*} offset   起始记录
 * @param {*} pageSize 每页多少条数据
 * @param {*} data {offset: '', pageSize: '', queryParam}
 */
export const fooddetails = (id) => {
    return axios({
        method: 'POST',
        url: axios.adornUrl('queryPageCooking/' + id),
        data: {
            queryParam: JSON.parse(sessionStorage.getItem('userMsg'))
        }
    })
}

/**
 * 导出作业excel表
 * @param {*} data 
 */
export const exportExecl = (data) => {
    return axios({
        method: 'post',
        url: axios.adornUrl('exportExecl'),
        responseType: 'blob',
        data: data
    })
}

/**
 * 获取体质,体检项目名称列表
 */
export const getHeaAndPhyProjectList = () => {
    return axios({
        method: 'post',
        url: axios.adornUrl('healthAndPhysicalList'),
        data: {
            queryParam: JSON.parse(sessionStorage.getItem('userMsg'))
        }
    })
}

/**
 * 获取年级
 */
export const getAllGardas = () => {
    const queryParam = JSON.parse(sessionStorage.getItem('userMsg'));
    var data = {};
    data.orgId = queryParam.orgCode;
    data.gradeType = localStorage.getItem('gradeType')
    return axios({
        method: 'GET',
        url: axios.wkbbBaseUrl('common/getGradeList'),
        params: data
    })
}

// 获取年级树
export const getClsTree = (data) => {
    return axios({
        method: 'GET',
        url: axios.wkbbBaseUrl('common/getClassTree'),
        params: data
    })
}

// ---------------------------------------------------作息管理-------------------------------------------
/**
 * 作息管理分页查询
 * @param {*} timeTable 体检列表项ID
 */
export const timeTable = (data) => {
    return axios({
        method: 'POST',
        url: axios.adornUrl('queryPageTimetable'),
        data: data
    })
}

// 考试管理 achievement/getExaminePageList
/**
 * 考试管理列表信息
 * @param {*} pageNo   页码
 * @param {*} pageSize 每页多少条数据
 */
export const exam_list = (data) => {
    return axios({
        method: 'GET',
        url: axios.wkbbBaseUrl('term/getTermPageList'),
        params: data
    })
}



// 删除考试管理 achievement/delTerm
/**
 * 删除考试管理列表信息
 * @param {*} pageNo   页码
 * @param {*} pageSize 每页多少条数据
 */

export const delete_exam_list = (data) => {
    return axios({
        method: 'DELETE',
        url: axios.wkbbBaseUrl('term/delTerm'),
        params: data
    })
}

// 新增考试项目 achievement/addTerm
/**
 * 新增考试项目
 * @param {*} pageNo   页码
 * @param {*} pageSize 每页多少条数据
 */

export const add_exam_list = (data) => {
    console.log('参数', data);
    return axios({
        method: 'POST',
        url: axios.wkbbBaseUrl('term/addTerm'),
        data: data
    })
}

// 考试项目详情 achievement/getTerm
/**
 * 考试项目详情
 * @param {*} pageNo   页码
 * @param {*} pageSize 每页多少条数据
 */

export const exam_list_details = (data) => {
    return axios({
        method: 'GET',
        url: axios.wkbbBaseUrl('term/getTerm'),
        params: data
    })
}

// 考试项目类型 achievement/getTermType
/**
 * 考试项目类型
 * @param {*} pageNo   页码
 * @param {*} pageSize 每页多少条数据
 */

export const get_exam_type = (data) => {
    return axios({
        method: 'GET',
        url: axios.wkbbBaseUrl('term/getTermType'),
        params: data
    })
}

// 修改考试项目 achievement/updateTerm
/**
 * 修改考试项目
 * @param {*} pageNo   页码
 * @param {*} pageSize 每页多少条数据
 */

export const update_exam_type = (data) => {
    return axios({
        method: 'PUT',
        url: axios.wkbbBaseUrl('term/updateTerm'),
        data: data
    })
}

// ---------------公共接口---------------------------
export const getCompany = () => {
    return axios({
        method: 'GET',
        url: axios.wkbbBaseUrl('common/getUnit')
    })
}


// 生成报告 report/getUserHlifeReport
/**
 * 生成报告
 * @param {*} pageNo   页码
 * @param {*} pageSize 每页多少条数据
 */

export const generate_report = (data) => {
    return axios({
        method: 'GET',
        url: axios.wkbbBaseUrl('report/getUserHlifeReport'),
        params: data
    })
}

// openUrl

// 根据学校班级id查询学生 
/**
 * 根据id查询学生
 * @param {*} pageNo   页码
 * @param {*} pageSize 每页多少条数据
 */

export const generate_people = (data) => {
    return axios({
        method: 'GET',
        url: axios.openUrl('student/getClassStudentList'),
        params: data
    })
}

//examine/updateExamine
// 修改 体检项目
/**
 * 根据id查询学生
 * @param {*} pageNo   页码
 * @param {*} pageSize 每页多少条数据
 */

export const update_examine = (data) => {
    return axios({
        method: 'PUT',
        url: axios.wkbbBaseUrl('examine/updateExamine'),
        data: data
    })
}



//examine/updateExamine
// 点击生成报告
/**
 * 根据id查询学生
 * @param {*} pageNo   页码
 * @param {*} pageSize 每页多少条数据
 */

export const bind_report = (data) => {
    return axios({
        method: 'GET',
        url: axios.wkbbBaseUrl('achievement/doTermAchievementScore'),
        params: data
    })
}

// export const bind_report = (data) => {
//     return axios({
//         method: 'GET',
//         url: axios.wkbbBaseUrl('report/getUserHlifeReport'),
//         params: data
//     })
// }

