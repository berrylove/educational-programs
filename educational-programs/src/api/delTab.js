// mainTabsActiveName: {
//     get() { returnstore.state.common.mainTabsActiveName },
//     set(val) { this.$store.commit('common/updateMainTabsActiveName', val) }
//   },
import store from '@/store'
import router from '@/router'
// 删除标签
export const removeTab = (tabName) => {
  // console.log('5555555', tabName,store.state.common.mainTabsActiveName,store.state.common.mainTabs);
  store.commit('common/updateMainTabs', store.state.common.mainTabs.filter(item => item.name !== tabName))
  // console.log('提出后的',store.state.common.mainTabs)
  if (store.state.common.mainTabs.length >= 1) {
    // 当前选中tab被删除
    if (tabName === store.state.common.mainTabsActiveName) {
      // console.log('if', tabName, store.state.common.mainTabsActiveName);
      var tab = store.state.common.mainTabs[store.state.common.mainTabs.length - 1] //拿到最后一个
      // console.log('最后一个的数据', tab);
      // 拿到最后一个并改变左边导航
      var names = tab.name;
      var menuList = JSON.parse(sessionStorage.getItem('menuList') || '[]');
      for (var i in menuList) {
        names == menuList[i].url ? menuList[i].is_default = 1 : menuList[i].is_default = 0
      }
     store.commit('common/updateMenuList', menuList);
      var arr = JSON.stringify(menuList)
      sessionStorage.setItem('menuList', arr);
      router.push({ name: tab.name, query: tab.query, params: tab.params }, () => {
        // console.log('this.$route', tab.name); // 最后一个的url
        store.commit('common/updateMainTabsActiveName', tab.name)
      })
    }
  } else {
    // console.log('elseif', tabName, store.state.common.mainTabsActiveName);
    store.commit('common/updateMenuActiveName', '')
    console.log('menuActiveName', store.state.common.menuActiveName);
    router.push({ name: 'home' });
    //没有导航回到
    var menuList = JSON.parse(sessionStorage.getItem('menuList') || '[]');
    for (var i in menuList) {
      menuList[i].is_default = 0;
      menuList[0].is_default = 1;
    }
   store.commit('common/updateMenuList', menuList);
    var arr = JSON.stringify(menuList)
    sessionStorage.setItem('menuList', arr)
  }
}