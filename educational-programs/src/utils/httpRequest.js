import Vue from 'vue'
import axios from 'axios'
// $$$$$$$$$$$$$
import router from '@/router'
import qs from 'qs'
import merge from 'lodash/merge'
import { clearLoginInfo } from '@/utils'
// 基地址
// const BASEURL = 'http://demo.open.renren.io/renren-fast-server'
// const BASEURL = 'http://47.97.220.184:8912/weisui'
const http = axios.create({
  timeout: 1000 * 30,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json; charset=utf-8;',
    // "Access-Control-Allow-Origin": "*",
    // "Access-Control-Allow-Headers": "X-Requested-With,Content-Type",
    // "Access-Control-Allow-Methods":"PUT,POST,GET,DELETE,OPTIONS"
  },
  // transformResponse: [function (data) {
  //   // 对 data 进行任意转换处理
  //   try {
  //     return JSONbigint.parse(data)
  //   } catch (err) {
  //     return data
  //   }
  // }]
  // transformRequest: [function (data) {
  //   // 对 data 进行任意转换处理
  //   try {
  //     // return JSON.stringify(data)
  //     // return JSON.parse(data)
  //   } catch (err) {
  //     return data
  //   }
  // }],
})

/**
 * 请求拦截
 */
http.interceptors.request.use(config => {
  // config.headers['token'] = Vue.cookie.get('token') // 请求头带上token
  config.headers['token'] = JSON.parse(sessionStorage.getItem('token'))
  // config.headers['Access-Control-Allow-Headers'] = 'Content-Type,Access-Token,Authorization,ybg'
  return config
}, error => {
  clearLoginInfo()
  return Promise.reject(error)
})

/**
 * 响应拦截
 */
http.interceptors.response.use(response => {
  if (response.data && response.data.code === 401) { // 401, token失效
    clearLoginInfo()
    // $$$$$$$$$$$$$
    router.push({ name: 'login' })
  }
  return response
}, error => {
  return Promise.reject(error)
})

/**
 * 登录请求地址处理
 * @param {*} actionName action方法名称
 */
http.userUrl = (actionName) => {
  // 非生产环境 && 开启代理, 接口前缀统一使用[/proxyApi/]前缀做代理拦截!
  return (process.env.NODE_ENV !== 'production' && process.env.OPEN_PROXY ? '/proxyApi/' : window.SITE_CONFIG.baseUrl) + actionName
  // return BASEURL + actionName
}

http.uuidUrl = (actionName) => {
  // 非生产环境 && 开启代理, 接口前缀统一使用[/proxyApi/]前缀做代理拦截!
  return (process.env.NODE_ENV !== 'production' && process.env.OPEN_PROXY ? '/proxyApi/' : window.SITE_CONFIG.uuid) + actionName
  // return BASEURL + actionName
}

// open
http.openUrl = (actionName) => {
  // 非生产环境 && 开启代理, 接口前缀统一使用[/proxyApi/]前缀做代理拦截!
  return (process.env.NODE_ENV !== 'production' && process.env.OPEN_PROXY ? '/proxyApi/' : window.SITE_CONFIG.open) + actionName
  // return BASEURL + actionName
}

// 体检
http.wkbbBaseUrl = (actionName) => {
  // 非生产环境 && 开启代理, 接口前缀统一使用[/proxyApi/]前缀做代理拦截!
  return (process.env.NODE_ENV !== 'production' && process.env.OPEN_PROXY ? '/proxyApi/' : window.SITE_CONFIG.base) + actionName
  // return BASEURL + actionName
}

/**
//  * 获取数据请求地址处理
//  * @param {*} actionName action方法名称
//  */
http.adornUrl = (actionName) => {
  // 非生产环境 && 开启代理, 接口前缀统一使用[/proxyApi/]前缀做代理拦截!
  return (process.env.NODE_ENV !== 'production' && process.env.OPEN_PROXY ? '/proxyApi/' : window.SITE_CONFIG.msgBaseUrl) + actionName
  // return BASEURL + actionName
}

/**
 * get请求参数处理
 * @param {*} params 参数对象
 * @param {*} openDefultParams 是否开启默认参数?
 */
http.adornParams = (params = {}, openDefultParams = true) => {
  var defaults = {
    //   't': new Date().getTime()
  }
  return openDefultParams ? merge(defaults, params) : params
}

/**
 * post请求数据处理
 * @param {*} data 数据对象
 * @param {*} openDefultdata 是否开启默认数据?
 * @param {*} contentType 数据格式
 *  json: 'application/json; charset=utf-8'
 *  form: 'application/x-www-form-urlencoded; charset=utf-8'
 */
http.adornData = (data = {}, openDefultdata = true, contentType = 'json') => {
  // var defaults = {
  //   't': new Date().getTime()
  // }
  // data = openDefultdata ? merge(defaults, data) : data
  return contentType === 'json' ? JSON.stringify(data) : qs.stringify(data)
}

export default http
