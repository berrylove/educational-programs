export default {
  namespaced: true,
  state: {
    // 页面文档可视高度(随窗口改变大小)
    documentClientHeight: 0,
    // 导航条, 布局风格, defalut(默认) / inverse(反向)
    navbarLayoutType: 'default',
    // 侧边栏, 布局皮肤, light(浅色) / dark(黑色)
    sidebarLayoutSkin: 'dark',
    // 侧边栏, 折叠状态
    sidebarFold: false,
    // 侧边栏, 菜单
    menuList: [],
    menuActiveName: '',
    // 内容, 是否需要刷新
    contentIsNeedRefresh: false,
    // 主入口标签页
    mainTabs: [],
    mainTabsActiveName: ''
  },
  mutations: {
    updateDocumentClientHeight(state, height) {
      state.documentClientHeight = height
    },
    updateNavbarLayoutType(state, type) {
      state.navbarLayoutType = type
    },
    updateSidebarLayoutSkin(state, skin) {
      state.sidebarLayoutSkin = skin
    },
    updateSidebarFold(state, fold) {
      state.sidebarFold = fold
    },
    updateMenuList(state, list) {

      state.menuList = list
    },
    updateMenuActiveName(state, name) {
      console.log('menuActiveName', name)
      state.menuActiveName = name
    },
    updateContentIsNeedRefresh(state, status) {
      state.contentIsNeedRefresh = status
    },
    updateMainTabs(state, tabs) {
      //存储页码栏的地方
      var brr = tabs;
      console.log('updateMainTabs', brr)

      for (var i in brr) {
        brr[i].changename ? '' : brr[i].changename = brr[i].title
      }
      state.mainTabs = brr;

    },
    updateMainTabsActiveName(state, name) {
      state.mainTabsActiveName = name
      // console.log('mainTabsActiveName',state.mainTabsActiveName); // 当前路径url
    },
    updateTitle(state, obj) {

      var brr = state.mainTabs
      var arr = brr.slice();
      for (var i in arr) {
        if (obj.type == 0) {
          obj.url == arr[i].name ? arr[i].title = arr[i].changename + '-新增' : ''
          console.log('新增');
          state.mainTabs = arr;
          // 新增
        } else if (obj.type == 1) {
          // 修改
          obj.url == arr[i].name ? arr[i].title = arr[i].changename + '-修改' : ''
          console.log('修改', obj.url, obj.type);
          state.mainTabs = arr;
        }
      }

    },
    // (作业管理-默认)标签跳转变成(作业管理)标签
    workTabPane(state, obj) {
      var brr = state.mainTabs
      var arr = brr.slice();
      var newArr = arr.filter(function (item) {
        return item.name.match(obj.url)
      })
      console.log(newArr);
      state.mainTabs = newArr;
    }
  }
}
