/**
 * 开发环境
 */
; (function () {
  window.SITE_CONFIG = {};

  // api接口请求地址
  // http://47.97.220.184:8912/weisui
  window.SITE_CONFIG['uuid'] = 'https://47.97.220.184:8912/weisui/';
  // window.SITE_CONFIG['baseUrl'] = 'http://localhost:8080/renren-fast';
  // window.SITE_CONFIG['msgBaseUrl'] = 'http://192.168.5.218:9021/sports/v1/';
  // 服务器
  window.SITE_CONFIG['baseUrl'] = 'https://newgateway.wkbaobao.com/api/open/v1/manage/';
  window.SITE_CONFIG['open'] = 'https://newgateway.wkbaobao.com/api/open/v1/';
  window.SITE_CONFIG['base'] = 'https://newgateway.wkbaobao.com/api/sports/v1/'; // wkbb线上基地址

  
  // window.SITE_CONFIG['baseUrl'] = 'https://gateway.wkbaobao.com/api/open/v1/manage/';
  // window.SITE_CONFIG['open'] = 'https://gateway.wkbaobao.com/api/open/v1/';
  // window.SITE_CONFIG['base'] = 'https://gateway.wkbaobao.com/api/sports/v1/'; // wkbb线上基地址
  

  // cdn地址 = 域名 + 版本号
  window.SITE_CONFIG['domain'] = './'; // 域名
  window.SITE_CONFIG['version'] = '';   // 版本号(年月日时分)
  window.SITE_CONFIG['cdnUrl'] = window.SITE_CONFIG.domain + window.SITE_CONFIG.version;
})();
