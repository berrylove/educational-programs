/**
 * 生产环境
 */
; (function () {
  window.SITE_CONFIG = {};

  // api接口请求地址
  // http://47.97.220.184:8912/weisui
  window.SITE_CONFIG['uuid'] = 'http://47.97.220.184:8912/weisui/';
  // window.SITE_CONFIG['baseUrl'] = 'http://demo.open.renren.io/renren-fast-server';
  // window.SITE_CONFIG['baseUrl'] = 'http://42.193.182.88:8080/';
  // window.SITE_CONFIG['baseUrl'] = 'https://newgateway.wkbaobao.com/api/';
  window.SITE_CONFIG['baseUrl'] = 'https://newgateway.wkbaobao.com/api/open/v1/manage/';
  window.SITE_CONFIG['open'] = 'https://newgateway.wkbaobao.com/api/open/v1/';
  window.SITE_CONFIG['base'] = 'https://newgateway.wkbaobao.com/api/sports/v1/';

  // window.SITE_CONFIG['baseUrl'] = 'https://gateway.wkbaobao.com/api/open/v1/manage/';
  // window.SITE_CONFIG['open'] = 'https://gateway.wkbaobao.com/api/open/v1/';
  // window.SITE_CONFIG['base'] = 'https://gateway.wkbaobao.com/api/sports/v1/'; // wkbb线上基地址
  
  // cdn地址 = 域名 + 版本号
  window.SITE_CONFIG['domain'] = './'; // 域名
  window.SITE_CONFIG['version'] = '';   // 版本号(年月日时分)
  window.SITE_CONFIG['cdnUrl'] = window.SITE_CONFIG.domain + window.SITE_CONFIG.version;
})();
